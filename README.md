# Continuous Integration

Линии сборки всех проектов группы Otus.

В корне нового проекта создаем файл `.gitlab-ci.yml` со следующим содержанием

```yaml
include:
  - project: $CI_PROJECT_ROOT_NAMESPACE/ci
    file: projects/$CI_PROJECT_NAME.yml
    ref: main
```

В текущем проекте создаем файл `projects/project-name.yml`, где `project-name` имя нового проекта.
Далее, для описания линий сборок, корректируются файлы в директории `projects`, а не проекты с исходным кодом.

В директория `cases` содержится конфигурация линий сборок повторного использования, например. сборка и выгрузка Docker образов.
